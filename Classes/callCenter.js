class CallCenter {
    constructor(sites = [], empleados = [], proveedores = []) {
        this.sites = sites;
        this.empleados = empleados;
        this.proveedores = proveedores;
        this.ventas = [];
        this.usuarios = [];
        this.notificacionesPortada = [];
        /*         this.usuarios = ls.get('usuarios'); */
    }
    
    getEmpleados() {
        this.actualizarEmpleados();
        return this.empleados;
    }
    actualizarEmpleados() {
        this.empleados = [];
        if (ls.get('empleados') != null)
            this.empleados = this.empleados.concat(ls.get('empleados'));
    }
    setPrimerEmpleado(empleado) {
        ls.set('empleados', empleado);
    }
    agregarEmpleado(empleado) {
        this.actualizarEmpleados();
        this.empleados.push(empleado);
        ls.set('empleados', this.empleados);
    }

    /*     nuevoEmpleado(nombre,apellido, dni, email, site) {
            let auxiliar = this.empleados.length;
            let empleado = new Empleado(auxiliar + 1, nombre, apellido, dni, email, site);
            this.setEmpleado(empleado);
        } */
    nuevoEmpleado(nombre, apellido, dni, email, site) {
        let id = this.empleados.length;
        if(this.buscarEmpleadoID(id+1)!=null)
            id=id-1;
        let empleado = new Empleado(id + 1, nombre, apellido, dni, email, site);
        if (this.empleados.length == 0)
            this.setPrimerEmpleado(empleado);
        else
            this.agregarEmpleado(empleado);
    }
    getSites() {
        return this.sites;
    }
    setSite(nombre, ubicacion = "") {
        let id = this.sites.length;
        this.sites.push(new Site(id, nombre, ubicacion));
    }
    getProveedores() {
        return this.proveedores;
    }
    setProveedor(nombre) {
        let id = this.proveedores.length;
        this.proveedores.push(new Proveedor(id, nombre));
    }
    actualizarVentas() {
        this.ventas = [];
        for (let empleado of this.empleados) {
            this.ventas = this.ventas.concat(empleado.getVentas());
        }
    }
    crearUsuario(empleado) {
        let nuevoUsuario = new Usuario(empleado);
        this.usuarios.push(nuevoUsuario);
    }
    buscarEmpleadoID(id) {
        try {
            let empleadoBuscado = this.empleados.find(e => e.id == id);
            return empleadoBuscado;
        } catch {
            console.error("No se encuentra el empleado");
            return null;
        }
    }
    eliminarEmpleadoID(id){
        let empleadoAEliminar = this.buscarEmpleadoID(id);
        let indice = this.empleados.indexOf(empleadoAEliminar);
        this.empleados.splice(indice,1);
        ls.set('empleados',this.empleados);
    }

    nuevaNotificacion(descripcion,imagen){
        let notificacionPortada = new NotificacionPortada(descripcion,imagen);
        this.agregarNotificacion(notificacionPortada);
    }
    
    actualizarNotificaciones() {
        this.notificacionesPortada = [];
        if (ls.get('notificacionesPortada') != null)
            this.notificacionesPortada = this.notificacionesPortada.concat(ls.get('notificacionesPortada'));
    }
    agregarNotificacion(notificacionPortada){
        this.actualizarNotificaciones();
        this.notificacionesPortada.push(notificacionPortada)
        ls.set('notificacionesPortada',this.notificacionesPortada);
    }
    getNotificacionesPortada(){
        this.actualizarNotificaciones();
        return this.notificacionesPortada;
    }
}

module.exports = CallCenter;
