class Plan{
    constructor(codigo,nombre,precioLista,beneficio){
    this.codigo = codigo;
    this.nombre = nombre;
    this.precioLista = precioLista;
    this.precioPromocion;
    this.beneficio = beneficio;
    this.descuento = 0;
    }
    getNombre(){
        return this.nombre;
    }
    getCodigo(){
        return this.codigo;
    }
    getPrecioVenta(){
        if(this.descuento>0)
            return this.precioPromocion;
        else
            return this.precioLista;
    }
    getPrecioLista(){
        return this.precioLista;
    }
    getBeneficios(){
        return this.beneficio;
    }
    getCantidadGigas(){
        return this.beneficio.getGigas();
    }
    getCantidadSMS(){
        return this.beneficio.getSMS();
    }
    getCantidadMinutos(){
        return this.beneficio.getMinutos();
    }
    aplicarDescuento(nuevoDescuento){
        this.descuento += nuevoDescuento.porcentaje;
        this.actualizarPrecio();
    }
    actualizarPrecio(){
        this.precioPromocion = this.precioLista-this.precioLista*this.descuento/100;
    }
    
}

var plan1 = new Plan (123,"Abono 2gb",100,new Beneficio(2,500,1000));