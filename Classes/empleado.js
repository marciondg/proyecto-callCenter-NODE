const Sancion = require("./sancion.js");

class Empleado {
    constructor(id, nombre, apellido, dni=0, email="noposee@noposee.com", site, estado="Activo") {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.email = email;
        this.rol;
        this.apercibimientos = [];
        this.supervisor;
        this.site = site;
        this.estado = estado;
        this.fechaIngreso = new Date()
        this.fechaNacimiento;
        this.fotoPerfil;
    }
    getNombreCompleto() {
        return `${this.nombre} ${this.apellido}`;
    }
    getNombre(){
        return this.nombre;
    }
    getApellido(){
        return this.apellido;
    }
    getID() {
        return this.id;
    }
    getDNI() {
        return this.dni;
    }
    getSite(){
        return this.site;
    }
    setSite(nuevoSite){
        this.site = nuevoSite;
    }
    setRol(nuevoRol) {
        this.rol = nuevoRol;
    }
    getRol() {
        return this.rol;
    }
    setSupervisor(nuevoSupervisor) {
        if(nuevoSupervisor.rol != "vendedor") // ver try/catch
            this.supervisor = nuevoSupervisor;
        else
            alert("El supervisor ingresado es un vendedor"); // servicio de notificacion.
    }
    getSupervisor() {
        return this.supervisor;
    }
    recibirApercibimiento(motivo="") {
        this.apercibimientos.push(new Sancion(this,motivo));
        this.chequearSuspension();
    }
    chequearSuspension() {
        if (this.apercibimientos.length >= 3)
            this.estado="Suspendido";
    }
    darBaja(){
        this.estado = "Baja";
    }
    activar(){
        this.estado = "Activo";
        this.apercibimientos = [];
    }
    getEstado(){
        return this.estado;
    }
    calcularAntiguedad(){
        /* return  */
    }
}

class Vendedor extends Empleado {
    constructor(id, nombre, apellido, dni,email) {
        super(id, nombre, apellido, dni,email);
        this.ventas = [];
        this.objetivo;
        this.rol = "vendedor";
    }
    getVentas(){
        return this.ventas;
    }
    vender(venta) {
        this.ventas.push(venta);
    }
    llegoObjetivo() {
        if (this.ventas.length >= objetivo)
            return true;
        else
            return false;
    }
    setObjetivo(nuevoObjetivo){
        this.objetivo=nuevoObjetivo;
    }
}

class Supervisor extends Empleado {
    constructor(id, nombre, apellido, dni,email) {
        super(id, nombre, apellido, dni,email);
        this.equipo = []; // Array de vendedores.
        this.rol="supervisor";
    }
    getEquipo() {
        return this.equipo;
    }
    
    setVendedorAlEquipo(vendedor) {
        this.equipo.push(vendedor)
    }
    
    apercibir(vendedor,motivo){
        if(this.equipo.includes(vendeor))
            vendedor.recibirApercibimiento(motivo);
        else
            alert("El vendedor no pertenece al equipo");
    }
}

module.exports = Empleado;
