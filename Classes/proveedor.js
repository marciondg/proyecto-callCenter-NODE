class Proveedor{
    constructor(id,nombre){
        this.id=id;
        this.nombre=nombre;
        this.listaPlanes= []; // Lista de Planes.
    }
    setPlan(plan){
        this.listaPlanes.push(plan);
    }
    getPlanes(){
        return this.listaPlanes;
    }
    getNombre(){
        return this.nombre;
    }
}

module.exports = Proveedor;