class Ubicacion{
    constructor(provincia,localidad,codigoPostal,calle,altura,piso="",departamento=""){
        this.provincia=provincia;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
        this.calle = calle;
        this.altura = altura;
        this.piso = piso;
        this.departamento =departamento;
    }
}

module.exports = Ubicacion;