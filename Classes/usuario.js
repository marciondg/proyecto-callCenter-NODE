class Usuario{
    constructor(empleado,contraseña="123456789"){
        this.empleado = empleado;
        this.userLogin= empleado.dni;
        this.contraseña = contraseña;
    }

    cambiarContraseña(nuevaContraseña){
        this.contraseña=nuevaContraseña;
    }
    getEmpleado(){
        return this.empleado;
    }
    getUserLogin(){
        return this.userLogin;
    }
}