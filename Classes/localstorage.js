class Ls {
    set(key, data) {
        try {
            localStorage.setItem(key, JSON.stringify(data));
        } catch (e) {
            console.error('Error Saving', e);
        }
    }

    get(key) {
        try {
            return JSON.parse(localStorage.getItem(key));
        } catch (e) {
            console.error('Error getting data', e);
            return null;
        }
    }

    remove(key) {
        try {
            localStorage.removeItem(key);
        } catch (e) {
            console.error('Error removing item', e);
        }
    }
    clear() {
        try {
            localStorage.clear();
        } catch (e) {
            console.error('Error cleaning localStorage', e);
        }
    }
}

const ls = new Ls();