class Cliente{
    constructor(linea,dni,nombre,proveedorActual){
        this.linea=linea;
        this.dni=dni;
        this.nombre=nombre;
        this.proveedorActual=proveedorActual;
    }
    getLinea(){
        return this.linea;
    }
    getDni(){
        return this.dni;
    }
    getNombre(){
        return this.nombre;
    }
    getProveedorActual(){
        return this.proveedorActual;
    }
}