class NotificacionPortada{
    constructor(descripcion,imagen){
        this.descripcion=descripcion;
        this.imagen=imagen;
    }

    getDescripcion(){
        return this.descripcion;
    }
    getImagen(){
        return this.imagen;
    }
    setDescripcion(descripcion){
        this.descripcion = descripcion;
    }
    setImagen(imagen){
        this.imagen = imagen;
    }
    
}