class Descuento{
    constructor(porcentaje){
        this.porcentaje=porcentaje;
    }
    getPorcentaje(){
        return this.porcentaje;
    }
    setPorcentaje(porcentaje){
        this.porcentaje = porcentaje;
    }
}
var descuento1 = new Descuento (50);