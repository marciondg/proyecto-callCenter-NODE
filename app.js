//Carga de modulos de clases
const Empleado = require('./Classes/empleado.js')
const Site = require('./Classes/site.js');
const CallCenter = require('./Classes/callCenter.js');
const Proveedor = require("./Classes/proveedor.js");
const Ubicacion = require("./Classes/ubicacion.js");


//Instanciar Sites
let centro0 = new Site (0,"Centro 0",new Ubicacion("Buenos Aires","CABA",1000,"Florida",537,6));
let centro1 = new Site (1,"Centro 1",new Ubicacion("Buenos Aires","CABA",1000,"Florida",537,5));
let centro2 = new Site (2,"Centro 2",new Ubicacion("Buenos Aires","CABA",1000,"Florida",537,13));
let oeste = new Site (3,"Oeste",new Ubicacion("Buenos Aires","Moron",1755,"Republica Oriental del Uruguay",27));
let norte = new Site (4,"Norte",new Ubicacion("Buenos Aires","Talar del Pacheco",1617,"Suiza",1120));

let arraySites = [centro0,centro1,centro2,oeste,norte];

//Instanciar empleados
let marcio = new Empleado (1,"Marcio","Garozzo",40346418,"mndgarozzo@hotmail.com",centro1);
let guillermo = new Empleado (2,"Guillermo","Russo",39434312,"guilleru@gmail.com",norte);
let hernanC = new Empleado (2,"Hernan","Caroglio",32321212,"hernanC@gmail.com",oeste);

let arrayEmpleados = [marcio,guillermo,hernanC];
//Instanciar Proveedores
let personal = new Proveedor(1,"Personal");
let mercadoLibre = new Proveedor(1,"Mercado Libre");

let arrayProveedores = [personal,mercadoLibre];

//Instanciar Call Center
let alcanzo = new CallCenter (arraySites,arrayEmpleados,arrayProveedores);

/* 
*****
*****
NODE
*****
*****
 */

 /* 
//Crear servidor
const http = require("http");
const host = 'localhost';
const port = 80;


// arrayRespuestas= [JSON.stringify(alcanzo),JSON.stringify(arraySites),JSON.stringify(arrayProveedores),JSON.stringify(arrayEmpleados)];
respuesta = JSON.stringify(alcanzo);
const requestListener = function(request,response){
    response.setHeader("Content-Type","application/json");
    response.setHeader("Access-Control-Allow-Origin","*"); //Permite acceso desde cualquier url.
    response.writeHead(200);
    response.end(respuesta);
}

const server = http.createServer(requestListener);

server.listen(port, host, () =>{
    console.log(`Server is running on http://${host}:${port}`);
});
 */

/* 
**************
 // EXPRESS //
**************
*/

const express = require('express')
const app = express()
const port = 3000


//========Direccionamientos básicos========

/* app.get('/', (req, res) => {
    res.send('Hello World!')
})


app.post('/', function (req, res) {
    res.send('Got a POST request');
});


app.put('/user', function (req, res) {
    res.send('Got a PUT request at /user');
});


app.delete('/user', function (req, res) {
    res.send('Got a DELETE request at /user');
});
 */


 //Direccionamientos GET

app.get('/',(req,res)=>{
    res.json(alcanzo)
})
app.get('/empleados',(req,res)=>{
    res.json(arrayEmpleados)
})
app.get('/sites',(req,res)=>{
    res.json(arraySites)
})

app.get('/proveedores',(req,res)=>{
    res.json(arrayProveedores)
})

//Manejo de respuestas 404
app.use(function(req, res, next) {
    res.status(404).send('Sorry cant find that!');
  });
  
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})

